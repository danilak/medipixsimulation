/**
\file
\brief Definitions of SimpleMedipixSimulator

This file contain definitions of class for simulating medipix detector
*/
#ifndef SimpleMedipixSimulator_h
#define SimpleMedipixSimulator_h 1

class SimpleMedipixSimulator {
private:
  /// Random numbers generator. TRandom3 with seed=0 is used. 
  TRandom * gRandom;
  /** 
  Pararameter A in formula for calculating radius of electron-hole cloud, produced as result of energy deposition in semiconductor:
  sigma=A*Energy*(1-B/(C*Energy+1))
  Energy in keV
  A=1.03 um/kev
  B=0.98
  C=0.003 1/kev
  */
  const float cloudA = 1.03;
  /** 
  Pararameter B in formula for calculating radius of electron-hole cloud, produced as result of energy deposition in semiconductor:
  sigma=A*Energy*(1-B/(C*Energy+1))
  Energy in keV
  A=1.03 um/kev
  B=0.98
  C=0.003 1/kev
  */
  const float cloudB = 0.98;
  /** 
  Pararameter C in formula for calculating radius of electron-hole cloud, produced as result of energy deposition in semiconductor:
  sigma=A*Energy*(1-B/(C*Energy+1))
  Energy in keV
  A=1.03 um/kev
  B=0.98
  C=0.003 1/kev
  */
  const float cloudC = 0.003;
  /// Size of pixel, only square pixels are supported.
  float pixelSize=55.0;
  /// Half size of pixel, only square pixels are supported.
  float halfPixelSize=22.5;
  /**
  Coefficient a in formula for transform charge to energy:
  energy=a*charge+b
  */
  float chargeToEnergyCoef = 1;
  /**
  Coefficient b in formula for transform charge to energy:
  energy=a*charge+b
  */
  float chargeToEnergyOffset = 0;
  /// Sensor thickness 
  float thickness = 300;
  /// Bias voltage applied to the sensor
  float uBias = 300;
  /// Energy threshold used in processHit function
  float thresholdMean = 6.0;
  /// Dispersion of energy threshold used in processHit function
  float thresholdDispersion = 0.001;
  /// Factor Fano used to smooth number of generated electron-hole pairs
  float factorFano = 0.1;
  /// Energy for production electron-hole pair
  float pairProductionEnergy = 0.0042;
  /// Charge collection efficiency used to smooth generated charge with Gaus function: charge=charge*Gaus(cceMean,cceDispersion)
  float cceMean = 0.9;
  /// Dispesion of charge collection efficiency used to smooth generated charge with Gaus function: charge=charge*Gaus(cceMean,cceDispersion)
  float cceDispersion = 0.01;
  /// Noise in preamplifier used to smooth generated charge with Gaus function: charge=Gaus(charge,preamplifierNoise)
  float preamplifierNoise = 100;
  /// Mismatch of preamplifier used to smooth generated charge with Gaus function: charge=Gaus(charge,charge*preamplifierMismatch)
  float preamplifierMismatch = 0.01;
  
  /// This function calculate radius of electron-hole pairs cloud produced as result of energy deposition in semiconductor.
  float GetElectronsCloudRadius(float energy);
  /// This function calculate number of electron-hole pairs produced as result of energy deposition in semiconductor.
  int GetNumberOfElectrons(float energy);
  /// This function return weight potential in point (x,y,z)
  float GetWeightPotential(float x, float y, float z);
  /** This function calculate sigma of diffusion for specified distance:
  sigmaDiffusion=sqrt(2*n*D*t)=0.398*distance/sqrt(uBias)
  , where n=3 - space dimension, uBias - bias voltage
  */
  float CalcSigmaDiffusion(float distance);

public:
  SimpleMedipixSimulator();
  ~SimpleMedipixSimulator();

  /// This function sets size of square pixel
  void SetPixelSize(float t_pixelSize);
  /// This function returns previously setted size of square pixel
  float GetPixelSize(); 
  /// This function sets bias voltage
  void SetUBias(float t_uBias);
  /// This function returns previously setted bias voltage
  float GetUBias();  
  /// This function sets detector thickness
  void SetThickness(float t_thickness);
  /// This function returns previously setted detector thickness
  float GetThickness();
  /// This function sets energy threshold
  void SetThreshold(float thr);
  /// This function returns previously setted detector thickness
  float GetThreshold();
  /// This function sets dispersion of energy threshold
  void SetThresholdDispersion(float t_thresholdDispersion);
  /// This function returns previously setted dispersion of energy threshold
  float GetThresholdDispersion();
  /// This function sets factor Fano
  void SetFactorFano(float t_factorFano);
  /// This function returns previously setted factor Fano
  float GetFactorFano();
  /// This function sets energy for production electron-hole pair
  void SetPairProductionEnergy(float t_pairProductionEnergy);
  /// This function returns previously setted energy for production electron-hole pair
  float GetPairProductionEnergy();
  /// This function sets charge collection efficiency
  void SetCce(float cce);
  /// This function returns previously setted charge collection efficiency
  float GetCce();
  /// This function sets dispersion of charge collection efficiency
  void SetCceDispersion(float cceDispersion);
  /// This function returns previously setted dispersion of charge collection efficiency
  float GetCceDispersion();
  /// This function sets preamplifier noise
  void SetPreamplifierNoise(float t_preamplifierNoise);
  /// This function returns previously setted preamplifier noise
  float GetPreamplifierNoise();
  /// This function sets preamplifier mismatch
  void SetPreamplifierMismatch(float t_preamplifierMismatch);
  /// This function returns previously setted preamplifier mismatch
  float GetPreamplifierMismatch();
  /// This function sets charge to energy calibration coefficient and offset
  void SetChargeToEnergyCalibration(float t_a, float t_b); 

  /// This function sets detector properties
  void SetProperties( float t_factorFano, float t_pairProductionEnergy, float t_cceMean, float t_cceDispersion,
                      float t_preamplifierNoise, float t_preamplifierMismatch, float t_thickness, float t_uBias,
                      float t_threshold, float t_thresholdDispersion);

  /// This is one of main functions which calculate generated charge for vector of energy deposites
  map<pair<int, int>, float > CalcGeneratedCharge(float x0, float y0, vector<float> * x, vector<float> * y, vector<float> * z, vector<float> * energy);
  /// This is one of main functions which process event for vector of energy deposites and produce vector of Hits for pixels with energy deposit
  map<pair<int, int>, Hit >  ProcessEvent(float x0, float y0, vector<float> * x, vector<float> * y, vector<float> * z, vector<float> * energy);
  
};

#endif