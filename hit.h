#ifndef Hit_h
#define Hit_h 1

class Hit {
public:
  bool count;
  float TOA;
  float TOT;
  float charge;

  Hit() {
    count = false;
    TOA = 0;
    TOT = 0;
    charge = 0;
  };
  ~Hit() {};
};

#endif