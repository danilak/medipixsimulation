#1. Prepare data 

$ cd ./build

$ cmake ..

$ ./game det.cfg RUN_NUMBER [ENERGY] [PARTICLE]


#2. Simulate medipix pixel

$ root -l

.L simpleMedipixSimulator2.C++

# For simulation of counts scan along X axis on synchrotron run:

ScanX(...);
