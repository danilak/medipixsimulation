// Software to simulate signal formation in the detector GaAs+Medipix (GAME)
// Initial version: A. Meneses 4/11/2015
//                  A.Zhemchugov 17/11/2015
// Modified: D.Kozhevnikov 12/01/17

#include "GAMeDetectorConstruction.hh"
#include "GAMeActionInitialization.hh"

#ifdef G4MULTITHREADED
#include "G4MTRunManager.hh"
#else
#include "G4RunManager.hh"
#endif

#include "G4UImanager.hh"
#include "QBBC.hh"

#include "GAMeConfigReader.hh"
#include "GAMePhysicsConstruction.hh"
#include "GAMeRootWriter.hh"

#include "G4VisExecutive.hh"
#include "G4UIExecutive.hh"

#include "Randomize.hh"

int main(int argc, char** argv)
{
  if (argc < 3 /*&& argc != 3*/)
  {
    G4cout << "Usage: ./mega detector.cfg RUN_NUMBER [ENERGY] [PARTICLE]" << G4endl;
    return 0;
  }

  int runNumber = 0;
  runNumber = atol(argv[2]);

  GAMeConfigReader::GetInstance()->ParseFile(argv[1]);

  std::cout << "* " << GAMeConfigReader::GetInstance()->GetCfgname() << std::endl;

  GAMeRootWriter::GetPointer()->Initialize(runNumber);

  // Choose the Random engine
  G4Random::setTheEngine(new CLHEP::RanecuEngine);

  // Construct the default run manager
#ifdef G4MULTITHREADED
  G4MTRunManager* runManager = new G4MTRunManager;
  runManager->SetNumberOfThreads(1);
  G4cout << "Number of threads:" << runManager->GetNumberOfThreads() << G4endl;
#else
  G4RunManager* runManager = new G4RunManager;
#endif

  // Set mandatory initialization classes
  runManager->SetUserInitialization(new GAMeDetectorConstruction());

  // Physics list
  G4VModularPhysicsList* physicsList = new GAMePhysicsConstruction;
  physicsList->SetVerboseLevel(1);
  physicsList->SetDefaultCutValue(5 * CLHEP::mm);
  physicsList->SetCuts();
  runManager->SetUserInitialization(physicsList);

  // User action initialization
  runManager->SetUserInitialization(new GAMeActionInitialization());

  runManager->Initialize();

  long rand[2];
  rand[0] = long(runNumber * 1000000 + 123456);
  rand[1] = 123456789;
  const long* rand1 = rand;
  CLHEP::HepRandom::setTheSeeds(rand1);

  // Get the pointer to the User Interface manager
  G4UImanager* UImanager = G4UImanager::GetUIpointer();
  UImanager->ApplyCommand("/process/em/deexcitation World true true true");
  UImanager->ApplyCommand("/process/em/fluo true");
  UImanager->ApplyCommand("/process/em/auger true");
  UImanager->ApplyCommand("/process/em/pixe true");

  if (argc == 2)
  {
    G4VisManager* visManager = new G4VisExecutive;
    visManager->Initialize();
    G4UIExecutive* ui = new G4UIExecutive(argc, argv);
    //  G4UIsession* ui = new G4UIterminal();
    UImanager->ApplyCommand("/control/execute init_vis.mac");
    ui->SessionStart();
    delete ui;
    delete visManager;
  }
  else if (argc > 2)
  {
    long evnb =  GAMeConfigReader::GetInstance()->GetLong("EVENTNB");
    //double energy =  GAMeConfigReader::GetInstance()->GetDouble("ENERGY");
    if (argc > 3) {
      char cmd[256];
      sprintf(cmd, "/gun/energy %f keV", atof(argv[3]));
      UImanager->ApplyCommand(cmd);
      if (argc > 4) {
        sprintf(cmd, "/gun/particle %s", atof(argv[4]));
        UImanager->ApplyCommand(cmd);
      }
    }
    runManager->BeamOn(evnb);
  }

  GAMeRootWriter::GetPointer()->Finalize();
  delete runManager;
}

