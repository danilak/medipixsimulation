#ifndef GAMeConfigReader_h
#define GAMeConfigReader_h

#include <string>
#include <map>

class GAMeConfigReader
{
public:
  
  static GAMeConfigReader* GetInstance();

  void ParseFile(std::string ConfigFileName);
  std::string GetString(std::string val);
  long GetLong(std::string val);
  double GetDouble(std::string val);
  bool GetBool(std::string val);

  std::string GetCfgname();

private:
  GAMeConfigReader();
  ~GAMeConfigReader();

  std::string m_fname;
  std::map<std::string,std::string> m_configs;

  static GAMeConfigReader* fgInstance;
};

#endif
