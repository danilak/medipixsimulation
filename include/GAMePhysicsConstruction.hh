#ifndef TGAMePhysicsConstruction_h
#define TGAMePhysicsConstruction_h 1

#include <CLHEP/Units/SystemOfUnits.h>

#include "globals.hh"
#include "G4VModularPhysicsList.hh"
#include "CompileTimeConstraints.hh"

template<class T>
class TGAMePhysicsConstruction: public T
{
public:
  TGAMePhysicsConstruction( G4int verbose = 0 );
  virtual ~TGAMePhysicsConstruction();
  
public:
  virtual void SetCuts();

private:
  enum {ok = CompileTimeConstraints::IsA<T, G4VModularPhysicsList>::ok };
};
#include "GAMePhysicsConstruction.icc"
typedef TGAMePhysicsConstruction<G4VModularPhysicsList> GAMePhysicsConstruction;

#endif



