#ifndef GAMeRootWriter_H
#define GAMeRootWriter_H 1

#include "TTree.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TH3D.h"
#include "TFile.h"
#include <string>
#include <vector>

class GAMeRootWriter{
  public:
    void Initialize(long runNumber);	
    void Fill();		// store an event
    void Finalize();	// write tree to a file
    static GAMeRootWriter* GetPointer();

  private:
    GAMeRootWriter(){};	// empty constructor
    static GAMeRootWriter* pInstance;
    
  public:
    TTree* tree;
    std::vector<float> * vecE;
    std::vector<float> * vecX;
    std::vector<float> * vecY;
    std::vector<float> * vecZ;
    float a;

  private:
    TFile* file;
};

#endif
