#include <iomanip>   

#include "globals.hh"
#include "G4ios.hh"
#include "G4ProcessManager.hh"
#include "G4ProcessVector.hh"
#include "G4ParticleTypes.hh"
#include "G4ParticleTable.hh"

#include "G4Material.hh"
#include "G4MaterialTable.hh"

#include "G4DecayPhysics.hh"
#include "G4RadioactiveDecayPhysics.hh"
#include "G4EmLivermorePhysics.hh"
#include "G4EmExtraPhysics.hh"
#include "G4IonQMDPhysics.hh"
#include "G4StoppingPhysics.hh"
#include "G4HadronElasticPhysicsHP.hh"
//#include "G4HadronElasticPhysicsLEND.hh"
#include "G4EmProcessOptions.hh"
#include "G4HadronPhysicsQGSP_BERT_HP.hh"

#include "G4LeptonConstructor.hh"
#include "G4MesonConstructor.hh"
#include "G4BaryonConstructor.hh"

#include "G4DataQuestionaire.hh"
//#include "HadronPhysicsShielding.hh"

//template<class T> TGAMePhysicsConstruction<T>::TGAMePhysicsConstruction(G4int ver):  T()
template<class T> TGAMePhysicsConstruction<T>::TGAMePhysicsConstruction( G4int verbose ):  T()
{
  // default cut value  (1.0mm) 
  // defaultCutValue = 1.0*CLHEP::mm;
  G4DataQuestionaire it(photon);
  G4cout << "<<< Geant4 Physics List simulation engine: GAMePhysicsConstruction 2.0"<<G4endl;
  G4cout <<G4endl;
  this->defaultCutValue = 0.7*CLHEP::mm;  
  this->SetVerboseLevel(verbose);

  G4LeptonConstructor lConstructor;
  lConstructor.ConstructParticle();
  G4MesonConstructor mConstructor;
  mConstructor.ConstructParticle();
  G4BaryonConstructor bConstructor;
  bConstructor.ConstructParticle();

 // EM Physics
  this->RegisterPhysics( new G4EmLivermorePhysics(verbose));

  G4EmProcessOptions emOptions;
  emOptions.SetBuildCSDARange(true);
  emOptions.SetDEDXBinningForCSDARange(10*10);
  //emOptions.SetDeexcitationActiveRegion(true); //TBC
  emOptions.SetFluo(true);
  emOptions.SetAuger(true);
  emOptions.SetPIXE(true);
  //emOptions.SetVerbose(3,"all");

  // Synchroton Radiation & GN Physics
  this->RegisterPhysics( new G4EmExtraPhysics(verbose) );

  // Decays 
  this->RegisterPhysics( new G4DecayPhysics(verbose) );
  //if ( rad == true ) this->RegisterPhysics( new G4RadioactiveDecayPhysics(verbose) );
  this->RegisterPhysics( new G4RadioactiveDecayPhysics(verbose) );

  // Stopping Physics
  this->RegisterPhysics( new G4StoppingPhysics(verbose) );

  // Ion Physics
  this->RegisterPhysics( new G4IonQMDPhysics(verbose));

  // HP Neutrons
  // Hadron Elastic scattering
  this-> RegisterPhysics( new G4HadronElasticPhysicsHP(verbose) );

  // Hadron Physics
  this->RegisterPhysics( new G4HadronPhysicsQGSP_BERT_HP(verbose));

}

template<class T> TGAMePhysicsConstruction<T>::~TGAMePhysicsConstruction()
{
}

template<class T> void TGAMePhysicsConstruction<T>::SetCuts()
{
  if (this->verboseLevel >1){
    G4cout << "GAMePhysicsConstruction::SetCuts:";
  }  
  //  " G4VUserPhysicsList::SetCutsWithDefault" method sets 
  //   the default cut value for all particle types 

  this->SetCutsWithDefault();   
 
  //if (this->verboseLevel > 0)
  //  G4VUserPhysicsList::DumpCutValuesTable();  
}
