/*
Created by Kozhevnikov Danila on 16.12.2016
*/
#include "TRandom3.h"
#include "TH2F.h"
#include "TH1F.h"
#include "TF1.h"
#include "TMath.h"
#include "TFile.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TGraphErrors.h"
#include "TStyle.h"
#include <iostream>
#include <algorithm>
#include <vector>
#include <map>
#include "math.h"
#include <fstream>
#include <stdio.h>
#include "hit.h"
#include "SimpleMedipixSimulator.h"
#include "SimpleMedipixSimulator.C"

using namespace std;


Double_t fitfunc(Double_t *x, Double_t *par) {
  Double_t xx;
  Double_t sum = 0;
  Int_t np = par[6];
  Double_t step = par[7];

  xx = x[0] ;
  Double_t z_a = (xx - par[1]) / (sqrt(2) * par[2]);
  Double_t z_b = (xx - par[9]) / (sqrt(2) * par[10]);
  sum = par[0] * (TMath::Gaus(xx, par[1], par[2]) + (par[3] / 2) * (1 + TMath::Erfc(z_a)))
        + par[4] * xx + par[5]
        + par[8] * (TMath::Gaus(xx, par[1] - par[9], par[10]) + (par[3] / 2) * (1 + TMath::Erfc(z_b)));

  return sum * step;
}

Double_t fitfunc2(Double_t *x, Double_t *par) {
  Double_t np;
  Double_t xx;
  Double_t sum = 0;
  Double_t xlow, xupp;
  Double_t step;
  Double_t i;

  np = par[6];
  step = par[7];

  for (i = 0; i < np; i++) {
    xx = x[0] + i * step;
    sum += fitfunc(&xx, par);
  }
  return sum;
}




/*
This procedure calculate charge to energy calibration curve.
It used mega_%energy%.root files for initial information of events.
For 5 energies charge histograms are filled and fitted.
*/
void chargeToEnergyCalibration(SimpleMedipixSimulator * detector, float collimatorWidth = 2, float collimatorHeight = 2, int NPhotons = 10000, bool saveFile = true) {
  // TCanvas * c0 = new TCanvas("chargeToEnergyCalibrationCanvas", "charge to energy calibration", 1200, 800);
  // c0->Divide(3, 2, 0.02, 0.02);
  // TCanvas * c1 = new TCanvas("chargeToEnergyCalibrationCanvasIntegral", "charge to energy calibration integral", 1200, 800);
  // c1->Divide(3, 2, 0.02, 0.02);

  TH1F * chargeToEnergyCalibrationHist[5];
  TH1F * chargeToEnergyCalibrationHistIntegral[5];

  Float_t energies[5];
  Float_t charges[5];
  Float_t energies_e[5];
  Float_t charges_e[5];
  for (int j = 0; j < 5; j++) {
    energies[j] = 10 + j * 4;
    charges[j] = 0;
    energies_e[j] = 0;
    charges_e[j] = 0;
  }
  vector<float > * vx;
  vector<float > * vy;
  vector<float > * vz;
  vector<float > * ve;
  vx = new vector<float>;
  vy = new vector<float>;
  vz = new vector<float>;
  ve = new vector<float>;

  for (int j = 0; j < 5; j++) {
    char name[256];
    sprintf(name, "energy_%d", j);
    char title[256];
    sprintf(title, "energy=%f", energies[j]);
    chargeToEnergyCalibrationHist[j] = new TH1F(name, title, 1000, 0, 10000);
    chargeToEnergyCalibrationHist[j]->GetXaxis()->SetTitle("charge");
    chargeToEnergyCalibrationHist[j]->GetYaxis()->SetTitle("hits");

    sprintf(name, "energy_%d_integral", (int)energies[j]);
    chargeToEnergyCalibrationHistIntegral[j] = new TH1F(name, title, 1000, 0, 10000);
    chargeToEnergyCalibrationHistIntegral[j]->GetXaxis()->SetTitle("charge");
    chargeToEnergyCalibrationHistIntegral[j]->GetYaxis()->SetTitle("hits");

    float x0 = 0;
    float y0 = 0;
    float x = 0;
    float y = 0;
    float z = 0;
    float q = 0;

    char fname[256];
    sprintf(fname, "./build/mega_%d.root",  (int)energies[j]);
    TFile * ft = new TFile(fname);
    TTree * megaTree = (TTree*)ft->Get("tree");
    megaTree->SetBranchAddress("x", &vx);
    megaTree->SetBranchAddress("y", &vy);
    megaTree->SetBranchAddress("z", &vz);
    megaTree->SetBranchAddress("e", &ve);
    int nEntries = megaTree->GetEntries();
    vector<int> entries(nEntries);
    for (int i = 0; i <  nEntries; i++) {
      entries[i] = i;
    }
    random_shuffle(entries.begin(), entries.end());
    for (int i = 0; i < min(NPhotons, nEntries); i++) {
      megaTree->GetEntry(entries[i % nEntries]);
      x = x0 + (gRandom->Rndm() - 0.5) * collimatorWidth;
      y = y0 + (gRandom->Rndm() - 0.5) * collimatorHeight;
      map<pair<int, int>, float > qs = detector->CalcGeneratedCharge(x, y, vx, vy, vz, ve);
      q = qs[make_pair(0, 0)];
      chargeToEnergyCalibrationHist[j]->Fill(q);
    }
    megaTree->Delete();
    ft->Close();
    ft->Delete();
  }
  delete vx;
  delete vy;
  delete vz;
  delete ve;

  Double_t initScale_a[5] = {8, 8, 8, 8, 8};
  Double_t minScale_a[5] = {0.1, 0.1, 0.1, 0.1, 0.1};
  Double_t maxScale_a[5] = {30, 30, 30, 30, 30};

  Double_t initMean_a[5] = {1600, 2200, 2800, 3400, 4300};
  Double_t minMean_a[5] = {1200, 2000, 2400, 3000, 3000};
  Double_t maxMean_a[5] = {1800, 2500, 3000, 3800, 4800};

  Double_t initSigma_a[5] = {150, 150, 150, 200, 200};
  Double_t minSigma_a[5] = {100, 100, 100, 100, 100};
  Double_t maxSigma_a[5] = {200, 200, 200, 400, 500};

  Double_t initCS_a[5] = {0.00000001, 0.00000001, 0.00000001, 0.00000001, 0.00000001};
  Double_t minCS_a[5] = {0.0, 0.0, 0.0, 0.0, 0.0};
  Double_t maxCS_a[5] = {0.01, 0.01, 0.01, 0.01, 0.01};

  Double_t initA[5] = { -0.001, -0.001, -0.001, -0.001, -0.001};
  Double_t minA[5] = { -0.01, -0.01, -0.01, -0.01, -0.01};
  Double_t maxA[5] = { 0.0, 0.0, 0.0, 0.0, 0.0};

  Double_t initB[5] = {0.01, 0.01, 0.01, 0.01, 0.01};
  Double_t minB[5] = {0.0, 0.0, 0.0, 0.0, 0.0};
  Double_t maxB[5] = {0.1, 0.1, 0.1, 0.1, 0.1};

  Double_t initScale_b[5] = {8, 8, 8, 8, 8};
  Double_t minScale_b[5] = {0, 0, 0, 0, 0};
  Double_t maxScale_b[5] = {30, 30, 30, 30, 30};

  Double_t initMeanShift_b[5] = {1600, 2200, 2800, 3400, 4300};
  Double_t minMeanShift_b[5] = {1200, 2000, 2400, 3000, 3000};
  Double_t maxMeanShift_b[5] = {1800, 2500, 3000, 3800, 4800};

  Double_t initSigma_b[5] = {150, 150, 150, 200, 200};
  Double_t minSigma_b[5] = {100, 100, 100, 100, 100};
  Double_t maxSigma_b[5] = {200, 200, 200, 400, 500};

  for (int j = 0; j < 5; j++) {
    chargeToEnergyCalibrationHistIntegral[j]->SetBinContent(chargeToEnergyCalibrationHist[j]->GetNbinsX() - 1,
        chargeToEnergyCalibrationHist[j]->GetBinContent(chargeToEnergyCalibrationHist[j]->GetNbinsX() - 1));
    Int_t maxBinContent = 0; Int_t maxBin = 0;
    Int_t rightEdge = chargeToEnergyCalibrationHist[j]->GetNbinsX() - 1;
    for (int k = chargeToEnergyCalibrationHist[j]->GetNbinsX() - 2; k >= 0; k--) {
      if (chargeToEnergyCalibrationHist[j]->GetBinContent(k) > maxBinContent) {
        maxBinContent = (chargeToEnergyCalibrationHist[j]->GetBinContent(k));
        maxBin = k;
      }
      if ((chargeToEnergyCalibrationHist[j]->GetBinContent(k) > 0) &&
          (rightEdge == chargeToEnergyCalibrationHist[j]->GetNbinsX() - 1)) {
        rightEdge = chargeToEnergyCalibrationHist[j]->GetBinCenter(k);
      }
      chargeToEnergyCalibrationHistIntegral[j]->SetBinContent(k,
          chargeToEnergyCalibrationHistIntegral[j]->GetBinContent(k + 1) + chargeToEnergyCalibrationHist[j]->GetBinContent(k));
      chargeToEnergyCalibrationHistIntegral[j]->SetBinError(k,
          max(sqrt(chargeToEnergyCalibrationHistIntegral[j]->GetBinContent(k + 1) + chargeToEnergyCalibrationHist[j]->GetBinContent(k)), 1.0));
    }
    Int_t peakPos = chargeToEnergyCalibrationHist[j]->GetBinCenter(maxBin);

    initMean_a[j] = rightEdge * 0.9;
    minMean_a[j] = rightEdge * 0.4;
    maxMean_a[j] = rightEdge;

    initSigma_a[j] = rightEdge * 0.1;
    minSigma_a[j] = 10;
    maxSigma_a[j] = rightEdge * 0.5;

    initMeanShift_b[j] = rightEdge * 0.3;
    minMeanShift_b[j] = 100;
    maxMeanShift_b[j] = rightEdge * 0.5;

    initSigma_b[j] = rightEdge * 0.1;
    minSigma_b[j] = 10;
    maxSigma_b[j] = rightEdge * 0.5;

    Float_t xFitIntLow = rightEdge * 0.5;
    Float_t xFitIntHigh = rightEdge * 1.1;
    Double_t np = 1000;
    Double_t step = 10;

    // cout << "rightEdge: " << rightEdge << endl;
    // cout << "xFitIntLow: " << xFitIntLow << endl;
    // cout << "xFitIntHigh: " << xFitIntHigh << endl;

    TF1 * f2 = new TF1("KneePixel2", fitfunc2, xFitIntLow, xFitIntHigh, 11);
    f2->SetParameters(initScale_a[j], initMean_a[j], initSigma_a[j], initCS_a[j], 0, 0, xFitIntLow, xFitIntHigh, initScale_b[j], initMeanShift_b[j], initSigma_b[j]);
    f2->SetParLimits(0, minScale_a[j], maxScale_a[j]);
    f2->SetParLimits(1, minMean_a[j], maxMean_a[j]);
    f2->SetParLimits(2, minSigma_a[j], maxSigma_a[j]);
    f2->SetParLimits(3, minCS_a[j], maxCS_a[j]);
    f2->FixParameter(4, 0);
    f2->FixParameter(5, 0);
    f2->FixParameter(6, np);
    f2->FixParameter(7, step);
    f2->SetParLimits(8, minScale_b[j], maxScale_b[j]);
    f2->SetParLimits(9, minMeanShift_b[j], maxMeanShift_b[j]);
    f2->SetParLimits(10, minSigma_a[j], maxSigma_a[j]);

    gStyle->SetOptStat(0);
    gStyle->SetOptFit(1);
    Int_t fitStatus = chargeToEnergyCalibrationHistIntegral[j]->Fit("KneePixel2", "R");
    fitStatus = chargeToEnergyCalibrationHistIntegral[j]->Fit("KneePixel2", "R");
    cout << "fitStatus: " << fitStatus << endl;
    Double_t par[11];
    f2->GetParameters(&par[0]);
    f2->Delete();

    TF1 * f1 = new TF1("KneePixel", fitfunc, xFitIntLow, xFitIntHigh, 11);
    f1->SetParameters(par);
    f1->SetParLimits(0, par[0] * 0.01, par[0] * 100);
    f1->SetParLimits(1, par[1] * 0.9, par[1] * 1.1);
    f1->SetParLimits(2, par[2] * 0.8, par[2] * 1.2);
    f1->FixParameter(3, 0);
    f1->FixParameter(4, 0);
    f1->FixParameter(5, 0);
    f1->FixParameter(6, np);
    f1->FixParameter(7, step);
    f1->SetParLimits(8, par[8] * 0.01, par[8] * 100);
    f1->SetParLimits(9, par[9] * 0.8, par[9] * 1.2);
    f1->SetParLimits(10, par[10] * 0.8, par[0] * 1.2);

    chargeToEnergyCalibrationHist[j]->Fit("KneePixel", "R");
    f1->GetParameters(&par[0]);
    f1->Delete();

    charges[j] = par[1];
    charges_e[j] = par[2];
    cout << "energy=" << energies[j] << " keV" << endl;
    cout << "mean charge=" << par[1] << " e" << endl;
    cout << "sigma charge=" << par[2] << " e" << endl;
    cout << "approximate resolution=" << par[2] / par[1] * 100 << "%" << endl;

    // c1->cd(j + 1);
    // chargeToEnergyCalibrationHistIntegral[j]->Draw();
    // c0->cd(j + 1);
    // chargeToEnergyCalibrationHist[j]->Draw();
  }


  //if (saveFile) {
  char name[1024];
  sprintf(name, "integralSpectras_d%0.1f_u%0.1f_cw%0.1f_ch%0.1f_ff%0.2f_cceM%0.2f_cceS%0.3f_pN%0.1f_pM%0.3f.root",
          detector->GetThickness(), detector->GetUBias(),   collimatorWidth,  collimatorHeight,    detector->GetFactorFano(),  detector->GetCce(),  detector->GetCceDispersion(), detector->GetPreamplifierNoise(), detector->GetPreamplifierMismatch() );
  TFile * fsave = new TFile(name, "RECREATE");
  fsave->cd();
  for (int j = 0; j < 5; j++) {
    chargeToEnergyCalibrationHistIntegral[j]->SetDirectory(fsave);
    chargeToEnergyCalibrationHistIntegral[j]->Write();
    chargeToEnergyCalibrationHist[j]->SetDirectory(fsave);
    chargeToEnergyCalibrationHist[j]->Write();
  }
  fsave->Close();
  fsave->Delete();
  //}

  float sx = 0;
  float sy = 0;
  float sxy = 0;
  float sxx = 0;
  for (int j = 0; j < 5; j++) {
    sx += charges[j];
    sy += energies[j];
    sxy += charges[j] * energies[j];
    sxx += charges[j] * charges[j];
  }

  float a = (5 * sxy - sx * sy) / (5 * sxx - sx * sx);
  float b = (sy - a * sx) / 5.0;

  detector->SetChargeToEnergyCalibration(a, b);

  char graphTitle[256];
  sprintf(graphTitle, "Calibration:   energy= %0.6f * charge + %0.6f", a, b);
  cout << graphTitle << endl;

  // c1->cd(6);
  // TGraphErrors * chargeToEnergyCalibrationGraph = new TGraphErrors(5, energies, charges, energies_e, charges_e);
  // chargeToEnergyCalibrationGraph->SetTitle(graphTitle);
  // chargeToEnergyCalibrationGraph->Draw("ALP*");
  // c0->Update();
  // c1->Update();
  // char name[1024];
  // sprintf(name, "chargeToEnergyCalibration_d%0.1f_u%0.1f_cw%0.1f_ch%0.1f_ff%0.2f_cceM%0.2f_cceS%0.3f_pN%0.1f_pM%0.3f.pdf",
  //         detector->GetThickness(), detector->GetUBias(),   collimatorWidth,  collimatorHeight,    detector->GetFactorFano(),  detector->GetCce(),  detector->GetCceDispersion(), detector->GetPreamplifierNoise(), detector->GetPreamplifierMismatch() );
  // c0->Print(name);
  // sprintf(name, "chargeToEnergyCalibrationIntegral_d%0.1f_u%0.1f_cw%0.1f_ch%0.1f_ff%0.2f_cceM%0.2f_cceS%0.3f_pN%0.1f_pM%0.3f.pdf",
  //         detector->GetThickness(), detector->GetUBias(),   collimatorWidth,  collimatorHeight,    detector->GetFactorFano(),  detector->GetCce(),  detector->GetCceDispersion(), detector->GetPreamplifierNoise(), detector->GetPreamplifierMismatch() );
  // c1->Print(name);
  // chargeToEnergyCalibrationGraph->Delete();
  // c0->Close();
}

void chargeToEnergyCalibration(int NPhotons = 10000, float thickness = 1000.0, float uBias = 500.0, float threshold = 6.0, float thresholdDispersion = 0., float collimatorWidth = 10.0, float collimatorHeight = 10.0, float factorFano = 0.1, float cceMean = 0.9, float cceSigma = 0.01, float preamplifierNoise = 100, float preamplifierMismatch = 0.01) {
  float pairProductionEnergy = 0.0042;
  SimpleMedipixSimulator * detector = new SimpleMedipixSimulator();
  detector->SetProperties(factorFano,  pairProductionEnergy, cceMean,  cceSigma,
                          preamplifierNoise,  preamplifierMismatch, thickness, uBias,  threshold,  thresholdDispersion);

  chargeToEnergyCalibration(detector, collimatorWidth, collimatorHeight, NPhotons, true);
  delete detector;
}

void ScanX(int NPhotons = 1000, float step = 5.0, float xLeftBound = -55.0, float xRightBound = 55.0, float thickness = 1000.0, float uBias = 500.0, float threshold = 6.0, float thresholdDispersion = 0.045, float y0 = 0, float collimatorWidth = 10.0, float collimatorHeight = 10.0, float energy = 18.0, float factorFano = 0.1, float cceMean = 0.9, float cceSigma = 0.01, float preamplifierNoise = 100, float preamplifierMismatch = 0.01, float a = -1, float b = -1, bool useCalibration = false) {
  TH1F * chargeHist = new TH1F("chargeHist", "generated charge distribution", 1000, 0, 4000);
  chargeHist->GetXaxis()->SetTitle("charge");
  chargeHist->GetYaxis()->SetTitle("hits");

  float pairProductionEnergy = 0.0042;
  SimpleMedipixSimulator * detector = new SimpleMedipixSimulator();
  detector->SetProperties(factorFano,  pairProductionEnergy, cceMean,  cceSigma,
                          preamplifierNoise,  preamplifierMismatch, thickness, uBias,  threshold,  thresholdDispersion);


  int NSteps = (xRightBound - xLeftBound) / step + 1;
  float halfStep = step * 0.5;
  cout << "NSteps=" << NSteps << endl;

  char name[1024];
  sprintf(name, "ScanXCounts_N%d_s%0.1f_xlb%0.1f_xrb%0.1f_d%0.1f_u%0.1f_thr%0.1f_thrD%0.1f_y%0.1f_cw%0.1f_ch%0.1f_e%0.1f_ff%0.2f_cceM%0.2f_cceS%0.3f_pN%0.1f_pM%0.3f.pdf",
          NPhotons, step,  xLeftBound,  xRightBound,  thickness, uBias, threshold,  thresholdDispersion, y0,  collimatorWidth,  collimatorHeight,  energy,  factorFano,  cceMean,  cceSigma, preamplifierNoise, preamplifierMismatch );

  TH1F * countsHist = new TH1F(name, name, NSteps, xLeftBound + 55, xLeftBound + step * NSteps + 55);
  countsHist->GetXaxis()->SetTitle("x");
  countsHist->GetYaxis()->SetTitle("counts");

  if (useCalibration) {
    detector->SetChargeToEnergyCalibration(a, b);
  } else {
    chargeToEnergyCalibration(detector, collimatorWidth, collimatorHeight);
  }

  float x0 = 0;
  float counts = 0;
  float charge = 0;
  float x = 0;
  float y = 0;
  float z = 0;

  TH1F ** spectra;
  spectra = new TH1F*[NSteps];
  char title[1024];
  for (int k = 0; k < NSteps; k++) {
    x0 = -55 + step * k + halfStep;
    sprintf(title, "x=%f", x0);
    spectra[k] = new TH1F(title, title, 100, 100, 4100);
  }

  vector<float > * vx;
  vector<float > * vy;
  vector<float > * vz;
  vector<float > * ve;
  vx = new vector<float>;
  vy = new vector<float>;
  vz = new vector<float>;
  ve = new vector<float>;

  for (int k = 0; k < NSteps; k++) {
    x0 = -55 + step * k /*+ halfStep*/;
    counts = 0;
    charge = 0;
    char fname[256];
    sprintf(fname, "./build/mega_%d.root", (int )energy);
    TFile * ft = new TFile(fname);
    TTree * megaTree = (TTree*)ft->Get("tree");
    megaTree->SetBranchAddress("x", &vx);
    megaTree->SetBranchAddress("y", &vy);
    megaTree->SetBranchAddress("z", &vz);
    megaTree->SetBranchAddress("e", &ve);
    int nEntries = megaTree->GetEntries();
    vector<int> entries(nEntries);
    for (int i = 0; i <  nEntries; i++) {
      entries[i] = i;
    }
    random_shuffle(entries.begin(), entries.end());
    for (int i = 0; i < NPhotons; i++) {
      megaTree->GetEntry(entries[i % nEntries]);
      //getPosition after collimator
      x = x0 + (gRandom->Rndm() - 0.5) * collimatorWidth;
      y = y0 + (gRandom->Rndm() - 0.5) * collimatorHeight;
      //process hit
      map<pair<int, int>, Hit > hits = detector->ProcessEvent(x, y, vx, vy, vz, ve);
      counts += hits[make_pair(0, 0)].count;
      charge = hits[make_pair(0, 0)].charge;
      spectra[k]->Fill(charge);
    }
    countsHist->SetBinContent(k, counts );
    megaTree->Delete();
    ft->Close();
    ft->Delete();
    cout << "k=" << k << "   counts=" << counts << "   charge=" << charge << endl;
  }
  countsHist->Scale(1.0 / NPhotons);
  delete vx;
  delete vy;
  delete vz;
  delete ve;

  TFile * fsave = new TFile("ScanXCounts.root", "UPDATE");
  fsave->cd();

  TCanvas * c3 = new TCanvas("countsCanvas", "counts", 800, 600);
  c3->cd();

  countsHist->SetMarkerStyle(21);
  countsHist->Draw("hist P");
  countsHist->SetDirectory(fsave);
  countsHist->Write();
  c3->Update();
  c3->Print(name);
  c3->Close();


  /*TCanvas * c4 = new TCanvas("spectraCanvas", "counts", 800, 600);
  c4->cd();
  for (int k = 0; k < NSteps; k++) {
    x0 = -55 + step * k + halfStep;
    spectra[k]->SetMarkerStyle(21);
    spectra[k]->Draw("hist P");
    spectra[k]->SetDirectory(fsave);
    spectra[k]->Write();
    c4->Update();
    sprintf(name, "spectra_%d_x%f_N%d_s%0.1f_xlb%0.1f_xrb%0.1f_sd%0.1f_thr%0.1f_thrD%0.1f_y%0.1f_cw%0.1f_ch%0.1f_e%0.1f_ff%0.2f_cceM%0.2f_cceS%0.3f_pN%0.1f_pM%0.3f.pdf",
            k, x0, NPhotons, step,  xLeftBound,  xRightBound,  sigmaDiffusion, threshold,  thresholdDispersion, y0,  collimatorWidth,  collimatorHeight,  energy,  factorFano,  cceMean,  cceSigma, preamplifierNoise, preamplifierMismatch );
    c4->Print(name);
  }*/
  //TODO: fit each spectra to find charge in peak

  delete detector;
  chargeHist->Delete();
  fsave->Close();
  fsave->Delete();
}



void Scan3X3Y(int NPhotons = 1000, float thickness = 300.0, float uBias = 300.0, float threshold = 6.0, float thresholdDispersion = 0.045,  float energy = 18.0, float factorFano = 0.1, float cceMean = 0.9, float cceSigma = 0.01, float preamplifierNoise = 100, float preamplifierMismatch = 0.01) {
  //may be based on ScanX, but with collimator size equals 3*pixelSize
  float collimatorWidth = 3 * 55;
  float collimatorHeight = 3 * 55;

  TH1F * chargeHist = new TH1F("chargeHist", "generated charge distribution", 100, 1000, 6000);
  chargeHist->GetXaxis()->SetTitle("charge");
  chargeHist->GetYaxis()->SetTitle("hits");

  float pairProductionEnergy = 0.0042;
  SimpleMedipixSimulator * detector = new SimpleMedipixSimulator();
  detector->SetProperties(factorFano,  pairProductionEnergy, cceMean,  cceSigma,
                          preamplifierNoise,  preamplifierMismatch, thickness, uBias,  threshold,  thresholdDispersion);



  char name[1024];
  sprintf(name, "pixelXScanCounts_N%d_d%0.1f_u%0.1f_thr%0.1f_thrD%0.1f_cw%0.1f_ch%0.1f_e%0.1f_ff%0.2f_cceM%0.2f_cceS%0.3f_pN%0.1f_pM%0.3f.pdf",
          NPhotons,   thickness, uBias, threshold,  thresholdDispersion,   collimatorWidth,  collimatorHeight,  energy,  factorFano,  cceMean,  cceSigma, preamplifierNoise, preamplifierMismatch );

  chargeToEnergyCalibration(detector);

  float x0 = 0;
  float y0 = 0;
  float counts = 0;
  float charge = 0;
  float x = 0;
  float y = 0;
  float z = 0;

  vector<float > * vx;
  vector<float > * vy;
  vector<float > * vz;
  vector<float > * ve;
  vx = new vector<float>;
  vy = new vector<float>;
  vz = new vector<float>;
  ve = new vector<float>;

  counts = 0;
  charge = 0;
  char fname[256];
  sprintf(fname, "./build/mega_%d.root", (int)energy);
  TFile * ft = new TFile(fname);
  TTree * megaTree = (TTree*)ft->Get("tree");
  megaTree->SetBranchAddress("x", &vx);
  megaTree->SetBranchAddress("y", &vy);
  megaTree->SetBranchAddress("z", &vz);
  megaTree->SetBranchAddress("e", &ve);
  int nEntries = megaTree->GetEntries();
  vector<int> entries(nEntries);
  for (int i = 0; i <  nEntries; i++) {
    entries[i] = i;
  }
  random_shuffle(entries.begin(), entries.end());
  for (int i = 0; i < NPhotons; i++) {
    megaTree->GetEntry(entries[i % nEntries]);
    //getPosition after collimator
    x = x0 + (gRandom->Rndm() - 0.5) * collimatorWidth;
    y = y0 + (gRandom->Rndm() - 0.5) * collimatorHeight;
    //process hit
    map<pair<int, int>, Hit > hits = detector->ProcessEvent(x, y, vx, vy, vz, ve);
    counts += hits[make_pair(0, 0)].count;
    charge = hits[make_pair(0, 0)].charge;
  }
  delete vx;
  delete vy;
  delete vz;
  delete ve;

  megaTree->Delete();
  ft->Close();
  ft->Delete();

  TFile * fsave = new TFile("Scan3X3Y.root", "UPDATE");
  fsave->cd();

  TCanvas * c2 = new TCanvas("chargeCanvas", "generated charge distribution", 800, 600);
  c2->cd();
  sprintf(name, "generatedChargeDistribution_N%d_d%0.1f_u%0.1f_thr%0.1f_thrD%0.1f_cw%0.1f_ch%0.1f_e%0.1f_ff%0.2f_cceM%0.2f_cceS%0.3f_pN%0.1f_pM%0.3f.pdf",
          NPhotons,   thickness, uBias, threshold,  thresholdDispersion,   collimatorWidth,  collimatorHeight,  energy,  factorFano,  cceMean,  cceSigma, preamplifierNoise, preamplifierMismatch );
  chargeHist->SetName(name);
  chargeHist->Draw();
  chargeHist->SetDirectory(fsave);
  chargeHist->Write();
  c2->Update();
  c2->Print(name);
  c2->Delete();

  delete detector;
  chargeHist->Delete();
  fsave->Close();
  fsave->Delete();
}


void ScanPixel(int NPhotons = 1000, float thickness = 300.0, float uBias = 300.0, float threshold = 6.0, float thresholdDispersion = 0.,  float energy = 18.0, float factorFano = 0.1, float cceMean = 0.9, float cceSigma = 0.01, float preamplifierNoise = 100, float preamplifierMismatch = 0.01) {
  float collimatorWidth = 55;
  float collimatorHeight = 55;

  TH1F * chargeHist = new TH1F("chargeHist", "generated charge distribution", 100, 1000, 6000);
  chargeHist->GetXaxis()->SetTitle("charge");
  chargeHist->GetYaxis()->SetTitle("hits");

  char name[1024];
  sprintf(name, "ScanPixelClusterSize_N%d_d%0.1f_u%0.1f_thr%0.1f_thrD%0.1f_cw%0.1f_ch%0.1f_e%0.1f_ff%0.2f_cceM%0.2f_cceS%0.3f_pN%0.1f_pM%0.3f.pdf",
          NPhotons,   thickness, uBias, threshold,  thresholdDispersion,   collimatorWidth,  collimatorHeight,  energy,  factorFano,  cceMean,  cceSigma, preamplifierNoise, preamplifierMismatch );
  TH1F * clusterSizeHist = new TH1F(name, name, 10, 0, 10);
  clusterSizeHist->GetXaxis()->SetTitle("cluster size");
  clusterSizeHist->GetYaxis()->SetTitle("hits");

  float pairProductionEnergy = 0.0042;
  SimpleMedipixSimulator * detector = new SimpleMedipixSimulator();
  detector->SetProperties(factorFano,  pairProductionEnergy, cceMean,  cceSigma,
                          preamplifierNoise,  preamplifierMismatch, thickness, uBias,  threshold,  thresholdDispersion);

  chargeToEnergyCalibration(detector);

  float x0 = 0;
  float y0 = 0;
  float counts = 0;
  float charge = 0;
  float x = 0;
  float y = 0;
  float z = 0;

  vector<float > * vx;
  vector<float > * vy;
  vector<float > * vz;
  vector<float > * ve;
  vx = new vector<float>;
  vy = new vector<float>;
  vz = new vector<float>;
  ve = new vector<float>;

  counts = 0;
  charge = 0;
  char fname[256];
  sprintf(fname, "./build/mega_%d.root", (int)energy);
  TFile * ft = new TFile(fname);
  TTree * megaTree = (TTree*)ft->Get("tree");
  megaTree->SetBranchAddress("x", &vx);
  megaTree->SetBranchAddress("y", &vy);
  megaTree->SetBranchAddress("z", &vz);
  megaTree->SetBranchAddress("e", &ve);
  int nEntries = megaTree->GetEntries();
  vector<int> entries(nEntries);
  for (int i = 0; i <  nEntries; i++) {
    entries[i] = i;
  }
  random_shuffle(entries.begin(), entries.end());
  for (int i = 0; i < NPhotons; i++) {
    megaTree->GetEntry(entries[i % nEntries]);
    //getPosition after collimator
    x = x0 + (gRandom->Rndm() - 0.5) * collimatorWidth;
    y = y0 + (gRandom->Rndm() - 0.5) * collimatorHeight;
    //process hit
    map<pair<int, int>, Hit > hits = detector->ProcessEvent(x, y, vx, vy, vz, ve);
    charge = hits[make_pair(0, 0)].charge;
    counts = 0;
    for (auto& it : hits) {
      counts += it.second.count;
    }
    clusterSizeHist->Fill(counts);
  }
  delete vx;
  delete vy;
  delete vz;
  delete ve;

  megaTree->Delete();
  ft->Close();
  ft->Delete();

  TFile * fsave = new TFile("ScanPixel.root", "UPDATE");
  fsave->cd();

  TCanvas * c2 = new TCanvas("chargeCanvas", "generated charge distribution", 800, 600);
  c2->cd();
  sprintf(name, "generatedChargeDistribution_N%d_d%0.1f_u%0.1f_thr%0.1f_thrD%0.1f_cw%0.1f_ch%0.1f_e%0.1f_ff%0.2f_cceM%0.2f_cceS%0.3f_pN%0.1f_pM%0.3f.pdf",
          NPhotons,   thickness, uBias, threshold,  thresholdDispersion,   collimatorWidth,  collimatorHeight,  energy,  factorFano,  cceMean,  cceSigma, preamplifierNoise, preamplifierMismatch );
  chargeHist->SetName(name);
  chargeHist->Draw();
  chargeHist->SetDirectory(fsave);
  chargeHist->Write();
  c2->Update();
  c2->Print(name);
  c2->Close();

  clusterSizeHist->SetDirectory(fsave);
  clusterSizeHist->Write();

  delete detector;
  chargeHist->Delete();
  clusterSizeHist->Delete();
  fsave->Close();
  fsave->Delete();
}



void ScanProbability(int NPhotons = 1000, float thickness = 300.0, float uBias = 300.0, float thresholdDispersion = 0., float factorFano = 0.1, float cceMean = 0.9, float cceSigma = 0.01, float preamplifierNoise = 100, float preamplifierMismatch = 0.01) {
  float collimatorWidth = 55;
  float collimatorHeight = 55;
  float pairProductionEnergy = 0.0042;
  SimpleMedipixSimulator * detector = new SimpleMedipixSimulator();
  detector->SetProperties(factorFano,  pairProductionEnergy, cceMean,  cceSigma,
                          preamplifierNoise,  preamplifierMismatch, thickness, uBias,  10,  thresholdDispersion);

  chargeToEnergyCalibration(detector);

  float x0 = 0;
  float y0 = 0;
  float counts = 0;
  float charge = 0;
  float x = 0;
  float y = 0;
  float z = 0;

  vector<float > * vx;
  vector<float > * vy;
  vector<float > * vz;
  vector<float > * ve;
  vx = new vector<float>;
  vy = new vector<float>;
  vz = new vector<float>;
  ve = new vector<float>;

  int minEnergy = 20;
  int maxEnergy = 100;
  int stepEnergy = 1;
  int nEnergies = (maxEnergy - minEnergy) / stepEnergy;

  int minThreshold = 10;
  int maxThreshold = 50;
  int stepThreshold = 5;
  int nThresholds = (maxThreshold - minThreshold) / stepThreshold;

  int minR = 0;
  int maxR = 60;
  int stepR = 5;
  int nRs = (maxR - minR) / stepR;

  int minAlpha = 0;
  int maxAlpha = 90;
  int stepAlpha = 5;
  int nAlphas = (maxAlpha - minAlpha) / stepAlpha;

  cout << nEnergies << " " << nThresholds << " " << nRs << " " << nAlphas << endl;

  charge = 0;
  char fname[512];
  //float probabilityArray[40][50][15][20] = {0};
  float**** probabilityArray = new float***[nEnergies];
  for (int i = 0; i < nEnergies; i++) {
    probabilityArray[i] = new float**[nThresholds];
    for (int j = 0; j < nThresholds; j++) {
      probabilityArray[i][j] = new float*[nRs];
      for (int k = 0; k < nRs; k++) {
        probabilityArray[i][j][k] = new float[nAlphas];
        for (int m = 0; m < nAlphas; m++) {
          probabilityArray[i][j][k][m] = 0;
        }
      }
    }
  }

  for (int i = 0; i < nEnergies; i ++) {
    int iEnergy = minEnergy + i * stepEnergy;
    sprintf(fname, "./build/mega_%d.root", iEnergy);
    TFile * ft = new TFile(fname);
    TTree * megaTree = (TTree*)ft->Get("tree");
    megaTree->SetBranchAddress("x", &vx);
    megaTree->SetBranchAddress("y", &vy);
    megaTree->SetBranchAddress("z", &vz);
    megaTree->SetBranchAddress("e", &ve);
    int nEntries = megaTree->GetEntries();
    vector<int> entries(nEntries);
    for (int ii = 0; ii <  nEntries; ii++) {
      entries[ii] = ii;
    }
    random_shuffle(entries.begin(), entries.end());
    for (int j = 0; j < /*min(iEnergy, */nThresholds; j++) {
      int iThreshold = minThreshold + j * stepThreshold;
      detector->SetThreshold(float(iThreshold));
      for (int k = 0; k < nRs; k++) {
        int iR = minR + k * stepR;
        for (int m = 0; m < nAlphas; m++) {
          int iAlpha = minAlpha + m * stepAlpha;
          cout << i << " " << j << " " << k << " " << m << endl;
          counts = 0;
          for (int i = 0; i < NPhotons; i++) {
            megaTree->GetEntry(entries[i % nEntries]);
            //getPosition after collimator
            float alpha = iAlpha + gRandom->Rndm() * stepAlpha;
            float r = iR + gRandom->Rndm() * stepR;
            x = r * cos(alpha);
            y = r * sin(alpha);
            //process hit
            map<pair<int, int>, Hit > hits = detector->ProcessEvent(x, y, vx, vy, vz, ve);
            counts += hits[make_pair(0, 0)].count;
          }
          probabilityArray[i][j][k][m] = counts * 1.0 / NPhotons;

        }
      }
    }
    megaTree->Delete();
    ft->Close();
    ft->Delete();
  }
  delete vx;
  delete vy;
  delete vz;
  delete ve;

  cout << "start writing data into file" << endl;
  FILE *probabilityFile;

  sprintf(fname, "probability_e_%d_%d_%d_t_%d_%d_%d_r_%d_%d_%d_a_%d_%d_%d.bin",
          minEnergy, maxEnergy, stepEnergy,
          minThreshold, maxThreshold, stepThreshold,
          minR, maxR, stepR,
          minAlpha, maxAlpha, stepAlpha);
  probabilityFile = fopen(fname, "wb");
  if (probabilityFile) {
    for (int i = 0; i < nEnergies; i ++) {
      for (int j = 0; j < nThresholds; j++) {
        for (int k = 0; k < nRs; k++) {
          for (int m = 0; m < nAlphas; m++) {
            fwrite(&probabilityArray[i][j][k][m], sizeof(float), 1, probabilityFile);
          }
        }
      }
    }
    fclose(probabilityFile);
  }
  cout << "finish writing data into file" << endl;

  delete detector;
}


void ScanClusterSize(int NPhotons = 100, float thickness = 300.0, float uBias = 300.0, float thresholdDispersion = 0., float factorFano = 0.1, float cceMean = 0.9, float cceSigma = 0.01, float preamplifierNoise = 100, float preamplifierMismatch = 0.01) {
  float collimatorWidth = 55;
  float collimatorHeight = 55;

  char fname[1024];
  char name[256];
  char title[256];

  float pairProductionEnergy = 0.0042;
  SimpleMedipixSimulator * detector = new SimpleMedipixSimulator();
  detector->SetProperties(factorFano,  pairProductionEnergy, cceMean,  cceSigma,
                          preamplifierNoise,  preamplifierMismatch, thickness, uBias,  10,  thresholdDispersion);

  chargeToEnergyCalibration(detector);

  float x0 = 0;
  float y0 = 0;
  int counts = 0;
  float charge = 0;
  float x = 0;
  float y = 0;
  float z = 0;

  vector<float > * vx;
  vector<float > * vy;
  vector<float > * vz;
  vector<float > * ve;
  vx = new vector<float>;
  vy = new vector<float>;
  vz = new vector<float>;
  ve = new vector<float>;

  int minEnergy = 20;
  int maxEnergy = 40;
  int stepEnergy = 1;
  int nEnergies = (maxEnergy - minEnergy) / stepEnergy;

  int minThreshold = 10;
  int maxThreshold = 40;
  int stepThreshold = 1;
  int nThresholds = (maxThreshold - minThreshold) / stepThreshold;

  cout << nEnergies << " " << nThresholds  << endl;

  charge = 0;

  TH2F * clusterSize[16];
  for (int i = 0; i < 16; i++) {
    sprintf(name, "ScanPixelClusterSize_%d_N%d_d%0.1f_u%0.1f_thrD%0.1f_ff%0.2f_cceM%0.2f_cceS%0.3f_pN%0.1f_pM%0.3f_minE%d_maxE%d_sE%d_minT%d_maxT%d_sT%d.pdf",
            i, NPhotons,   thickness, uBias,  thresholdDispersion, factorFano,  cceMean,
            cceSigma, preamplifierNoise, preamplifierMismatch,
            minEnergy, maxEnergy, stepEnergy, minThreshold, maxThreshold, stepThreshold);

    sprintf(title, "title");
    clusterSize[i] = new TH2F(name, title, nThresholds, minThreshold, maxThreshold, nEnergies, minEnergy, maxEnergy);
  }


  for (int i = 0; i < nEnergies; i ++) {
    int iEnergy = minEnergy + i * stepEnergy;
    sprintf(fname, "./build/mega_%d.root", iEnergy);
    TFile * ft = new TFile(fname);
    TTree * megaTree = (TTree*)ft->Get("tree");
    megaTree->SetBranchAddress("x", &vx);
    megaTree->SetBranchAddress("y", &vy);
    megaTree->SetBranchAddress("z", &vz);
    megaTree->SetBranchAddress("e", &ve);
    int nEntries = megaTree->GetEntries();
    vector<int> entries(nEntries);
    for (int ii = 0; ii <  nEntries; ii++) {
      entries[ii] = ii;
    }
    random_shuffle(entries.begin(), entries.end());
    for (int j = 0; j < min(iEnergy, nThresholds); j++) {
      int iThreshold = minThreshold + j * stepThreshold;
      detector->SetThreshold(float(iThreshold));
      cout << i << " " << j <<  endl;
      for (int i = 0; i < NPhotons; i++) {
        megaTree->GetEntry(entries[i % nEntries]);
        //getPosition after collimator
        x = (gRandom->Rndm() - 0.5) * collimatorWidth;
        y = (gRandom->Rndm() - 0.5) * collimatorHeight;
        //process hit
        map<pair<int, int>, Hit > hits = detector->ProcessEvent(x, y, vx, vy, vz, ve);
        counts = 0;
        for (auto& it : hits) {
          counts += it.second.count;
        }
        clusterSize[counts]->Fill(iThreshold, iEnergy);
      }
    }
    megaTree->Delete();
    ft->Close();
    ft->Delete();
  }

  TFile * fsave = new TFile("ScanClusterSize.root", "UPDATE");
  fsave->cd();

  for (int i = 0; i < 16; i++) {
    clusterSize[i]->Scale(1.0 / NPhotons);
    sprintf(name, "name");
    sprintf(title, "title");
    clusterSize[i]->SetDirectory(fsave);
    clusterSize[i]->Write();
  }

  delete detector;
  for (int i = 0; i < 16; i++) {
    clusterSize[i]->Delete();
  }
  fsave->Close();
  fsave->Delete();
}


void ScanThreshold(int NPhotons = 100, float thickness = 300.0, float uBias = 300.0, float energy = 18.0, float minThreshold = 10, float maxThreshold = 25, float stepThreshold = 0.5, float thresholdDispersion = 0., float factorFano = 0.1, float cceMean = 0.9, float cceSigma = 0.01, float preamplifierNoise = 100, float preamplifierMismatch = 0.01) {
  float collimatorWidth = 55;
  float collimatorHeight = 55;

  char fname[1024];
  char name[256];
  char title[256];

  float pairProductionEnergy = 0.0042;
  SimpleMedipixSimulator * detector = new SimpleMedipixSimulator();
  detector->SetProperties(factorFano,  pairProductionEnergy, cceMean,  cceSigma,
                          preamplifierNoise,  preamplifierMismatch, thickness, uBias,  10,  thresholdDispersion);

  chargeToEnergyCalibration(detector);

  float x0 = 0;
  float y0 = 0;
  int counts = 0;
  float charge = 0;
  float x = 0;
  float y = 0;
  float z = 0;

  vector<float > * vx;
  vector<float > * vy;
  vector<float > * vz;
  vector<float > * ve;
  vx = new vector<float>;
  vy = new vector<float>;
  vz = new vector<float>;
  ve = new vector<float>;


  int nThresholds = (maxThreshold - minThreshold) / stepThreshold;
  cout << "Thresholds: " << nThresholds  << endl;

  sprintf(name, "countsVsThreshold_N%d_d%0.1f_u%0.1f_e%0.0f_thrD%0.1f_ff%0.2f_cceM%0.2f_cceS%0.3f_pN%0.1f_pM%0.3f_minT%0.1f_maxT%0.1f_sT%0.1f.pdf",
          NPhotons,   thickness, uBias, energy,  thresholdDispersion, factorFano,  cceMean,
          cceSigma, preamplifierNoise, preamplifierMismatch,
          minThreshold, maxThreshold, stepThreshold);
  TH1F * countsVsThreshold = new TH1F(name, "countsVsThreshold", nThresholds + 1, minThreshold, minThreshold + nThresholds * stepThreshold);

  int iEnergy = (int) energy;
  sprintf(fname, "./build/mega_%d.root", iEnergy);
  TFile * ft = new TFile(fname);
  TTree * megaTree = (TTree*)ft->Get("tree");
  megaTree->SetBranchAddress("x", &vx);
  megaTree->SetBranchAddress("y", &vy);
  megaTree->SetBranchAddress("z", &vz);
  megaTree->SetBranchAddress("e", &ve);
  int nEntries = megaTree->GetEntries();
  vector<int> entries(nEntries);
  for (int ii = 0; ii <  nEntries; ii++) {
    entries[ii] = ii;
  }
  random_shuffle(entries.begin(), entries.end());
  for (int j = 0; j < nThresholds; j++) {
    float iThreshold = minThreshold + j * stepThreshold;
    detector->SetThreshold(float(iThreshold));

    counts = 0;
    for (int i = 0; i < NPhotons; i++) {
      megaTree->GetEntry(entries[i % nEntries]);
      //getPosition after collimator
      x = (gRandom->Rndm() - 0.5) * collimatorWidth;
      y = (gRandom->Rndm() - 0.5) * collimatorHeight;
      //process hit
      map<pair<int, int>, Hit > hits = detector->ProcessEvent(x, y, vx, vy, vz, ve);

      for (auto& it : hits) {
        counts += it.second.count;
      }
    }
    countsVsThreshold->SetBinContent(countsVsThreshold->FindBin(iThreshold), counts);
    cout << "threshold: " << iThreshold << "   counts: " << counts << endl;
  }
  megaTree->Delete();
  ft->Close();
  ft->Delete();


  TFile * fsave = new TFile("ScanThreshold.root", "UPDATE");
  fsave->cd();

  sprintf(name, "countsVsThreshold_N%d_d%0.1f_u%0.1f_e%0.0f_thrD%0.1f_ff%0.2f_cceM%0.2f_cceS%0.3f_pN%0.1f_pM%0.3f_minT%0.1f_maxT%0.1f_sT%0.1f.pdf",
          NPhotons,   thickness, uBias, energy,  thresholdDispersion, factorFano,  cceMean,
          cceSigma, preamplifierNoise, preamplifierMismatch,
          minThreshold, maxThreshold, stepThreshold);
  sprintf(title, "countsVsThreshold");
  countsVsThreshold->SetDirectory(fsave);
  countsVsThreshold->Write();
  countsVsThreshold->Delete();
  fsave->Close();
  fsave->Delete();

  delete detector;
}