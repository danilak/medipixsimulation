//

#include "GAMeActionInitialization.hh"
#include "GAMePrimaryGeneratorAction.hh"
#include "GAMeRunAction.hh"
#include "GAMeEventAction.hh"
#include "GAMeSteppingAction.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GAMeActionInitialization::GAMeActionInitialization()
 : G4VUserActionInitialization()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GAMeActionInitialization::~GAMeActionInitialization()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GAMeActionInitialization::BuildForMaster() const
{
  SetUserAction(new GAMeRunAction);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GAMeActionInitialization::Build() const
{
  SetUserAction(new GAMePrimaryGeneratorAction);
  SetUserAction(new GAMeRunAction);
  
  GAMeEventAction* eventAction = new GAMeEventAction;
  SetUserAction(eventAction);
  
  SetUserAction(new GAMeSteppingAction(eventAction));
}  

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
