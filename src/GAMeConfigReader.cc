#include "GAMeConfigReader.hh"

#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <algorithm>

using namespace std;

GAMeConfigReader* GAMeConfigReader::fgInstance = 0;

GAMeConfigReader* GAMeConfigReader::GetInstance()
{
  if (! fgInstance) fgInstance = new GAMeConfigReader();
  return fgInstance;
}

GAMeConfigReader::GAMeConfigReader()
{
  m_configs.clear();
}

GAMeConfigReader::~GAMeConfigReader()
{
  m_configs.clear();
}

std::string GAMeConfigReader::GetCfgname()
{
  std::size_t found = m_fname.find(".");
  return m_fname.substr(0, found);
}

void GAMeConfigReader::ParseFile(std::string ConfigFileName)
{
  GetInstance();
  m_fname = ConfigFileName;
  ifstream ifile;
  ifile.open(m_fname.c_str());
  if (! ifile)
  {
    cerr << "Unable open file " << m_fname << " for reading. Exiting" << endl;
    exit(-1);
  }
  std::string line;
  while (std::getline(ifile, line))
  {
    if (line == "" || (line[0] == '/' && line[1] == '/')) continue;
    std::string flag;
    std::string value;
    std::string tmp;
    istringstream istream(line);
    istream >> flag >> tmp >> value;
    if (flag == "" || value == "" || tmp != ":")
    {
      cerr << "Cannot parse config file: FLAG=" << flag << " VALUE=" << value << endl;
      exit(-1);
    }

    unsigned int strBegin = value.find_first_not_of(" \"");
    if (strBegin == std::string::npos)
    {
      m_configs[flag] = "";
      continue;
    }

    unsigned int strEnd = value.find_last_not_of(" \"");
    unsigned int strRange = strEnd - strBegin + 1;

    std::string val = value.substr(strBegin, strRange);

    m_configs[flag] = val;
  }
  ifile.close();
}

std::string GAMeConfigReader::GetString(std::string val)
{
  return m_configs[val];
}

long GAMeConfigReader::GetLong(std::string val)
{
  return atol(m_configs[val].c_str());
}

double GAMeConfigReader::GetDouble(std::string val)
{
  return atof(m_configs[val].c_str());
}

bool GAMeConfigReader::GetBool(std::string val)
{
  std::string str = m_configs[val].c_str();
  std::transform(str.begin(), str.end(), str.begin(), ::tolower);
  std::istringstream is(str);
  bool b;
  is >> std::boolalpha >> b;
  return b;
}

