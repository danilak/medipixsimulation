#include "GAMeEventAction.hh"
#include "GAMeRun.hh"

#include "G4Event.hh"
#include "G4RunManager.hh"

#include "GAMeConfigReader.hh"

//#include "simpleMedipixSimulator.hh"


GAMeEventAction::GAMeEventAction()
	: G4UserEventAction(),
	  fEdep(0.)
{
	rw = GAMeRootWriter::GetPointer();
	//rw->tree = new TTree("tree", "tree");
	//detector = new SimpleMedipixSimulator();
}

GAMeEventAction::~GAMeEventAction()
{
	//rw->Write()
}

void GAMeEventAction::BeginOfEventAction(const G4Event*)
{
	fEdep = 0.;
	GAMeRootWriter::GetPointer()->vecE->clear();
	GAMeRootWriter::GetPointer()->vecX->clear();
	GAMeRootWriter::GetPointer()->vecY->clear();
	GAMeRootWriter::GetPointer()->vecZ->clear();
	//clear
	//rw
}

void GAMeEventAction::EndOfEventAction(const G4Event*)
{
	// accumulate statistics in GAMeRun
	GAMeRun* run
	    = static_cast<GAMeRun*>(
	          G4RunManager::GetRunManager()->GetNonConstCurrentRun());
	run->AddEdep(fEdep);

	rw->Fill();
	/*for (auto& it : *(GAMeRootWriter::GetPointer()->vecE)) {
		std::cout << it << std::endl;
	}*/
}

void GAMeEventAction::AddEdep(G4double edep, G4ThreeVector pos) {
	fEdep += edep*CLHEP::eV;
	GAMeRootWriter::GetPointer()->vecE->push_back(edep);
	GAMeRootWriter::GetPointer()->vecX->push_back(pos.getX());
	GAMeRootWriter::GetPointer()->vecY->push_back(pos.getY());
	GAMeRootWriter::GetPointer()->vecZ->push_back(pos.getZ());

}

