//

#include "GAMeRunAction.hh"
#include "GAMePrimaryGeneratorAction.hh"
#include "GAMeDetectorConstruction.hh"
#include "GAMeRun.hh"

#include "G4RunManager.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4LogicalVolume.hh"
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GAMeRunAction::GAMeRunAction()
: G4UserRunAction()
{ 
  // add new units for dose
  // 
  const G4double kiloelectronvolt = 1.e-3*electronvolt;
  const G4double megaelectronvolt = 1.e-6*electronvolt;
     
  new G4UnitDefinition("kiloelectronvolt", "keV" , "Energy", kiloelectronvolt);
  new G4UnitDefinition("megaelectronvolt", "MeV" , "Energy", megaelectronvolt);
          
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GAMeRunAction::~GAMeRunAction()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Run* GAMeRunAction::GenerateRun()
{
  return new GAMeRun; 
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GAMeRunAction::BeginOfRunAction(const G4Run*)
{ 
  //inform the runManager to save random number seed
  G4RunManager::GetRunManager()->SetRandomNumberStore(false);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void GAMeRunAction::EndOfRunAction(const G4Run* run)
{
  G4int nofEvents = run->GetNumberOfEvent();
  if (nofEvents == 0) return;
  
  const GAMeRun* d1Run = static_cast<const GAMeRun*>(run);

  G4double edep  = d1Run->GetEdep();
  G4double edep2 = d1Run->GetEdep2();
  G4double rms = edep2 - edep*edep/nofEvents;
  if (rms > 0.) rms = std::sqrt(rms); else rms = 0.;

  const GAMeDetectorConstruction* detectorConstruction
   = static_cast<const GAMeDetectorConstruction*>
     (G4RunManager::GetRunManager()->GetUserDetectorConstruction());
  
  G4double energy = edep/nofEvents;

  // Run conditions
  //  note: There is no primary generator action object for "master"
  //        run manager for multi-threaded mode.
  const GAMePrimaryGeneratorAction* generatorAction
   = static_cast<const GAMePrimaryGeneratorAction*>
     (G4RunManager::GetRunManager()->GetUserPrimaryGeneratorAction());
  G4String runCondition;
  if (generatorAction)
  {
    const G4ParticleGun* particleGun = generatorAction->GetParticleGun();
    runCondition += particleGun->GetParticleDefinition()->GetParticleName();
    runCondition += " of ";
    G4double particleEnergy = particleGun->GetParticleEnergy();
    runCondition += G4BestUnit(particleEnergy,"Energy");
  }
        
  // Print
  //  
  if (IsMaster()) {
    G4cout
     << G4endl
     << "--------------------End of Global Run-----------------------";
  }
  else {
    G4cout
     << G4endl
     << "--------------------End of Local Run------------------------";
  }
  
  G4cout
     << G4endl
     << " The run consists of " << nofEvents << " "<< runCondition
     << G4endl
     << " Deposited energy in scoring volume : " 
     << G4BestUnit(energy,"Energy")
     << G4endl
     << "------------------------------------------------------------"
     << G4endl
     << G4endl;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......