#include "GAMeDetectorConstruction.hh"

#include "G4RunManager.hh"
#include "G4NistManager.hh"
#include "G4Box.hh"
#include "G4Cons.hh"
#include "G4Orb.hh"
#include "G4Sphere.hh"
#include "G4Trd.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4SystemOfUnits.hh"
#include "G4UserLimits.hh"


GAMeDetectorConstruction::GAMeDetectorConstruction()
  : G4VUserDetectorConstruction(),
    fScoringVolume(0)
{ }

GAMeDetectorConstruction::~GAMeDetectorConstruction()
{ }

G4VPhysicalVolume* GAMeDetectorConstruction::Construct()
{
  // Get nist material manager
  G4NistManager* nist = G4NistManager::Instance();

  // Option to switch on/off checking of volumes overlaps
  G4bool checkOverlaps = true;

  //
  // World
  //
  G4double world_sizeXY = 2 * cm;
  G4double world_sizeZ  = 18 * cm;
  G4Material* world_mat = nist->FindOrBuildMaterial("G4_AIR");

  G4Box* solidWorld =
    new G4Box("World",                               //its name
              world_sizeXY, world_sizeXY, world_sizeZ);     //its size

  G4LogicalVolume* logicWorld =
    new G4LogicalVolume(solidWorld,          //its solid
                        world_mat,           //its material
                        "World");            //its name

  G4VPhysicalVolume* physWorld =
    new G4PVPlacement(0,                     //no rotation
                      G4ThreeVector(),       //at (0,0,0)
                      logicWorld,            //its logical volume
                      "World",               //its name
                      0,                     //its mother  volume
                      false,                 //no boolean operation
                      0,                     //copy number
                      checkOverlaps);        //overlaps checking


  G4double z, a, fractionmass, density;
  G4String name, symbol;
  G4int ncomponents;

  a = 69.723 * g / mole;
  G4Element* elGa = new G4Element (name = "Galium", symbol = "Ga", z = 31., a);

  a = 74.92 * g / mole;
  G4Element* elAs = new G4Element (name = "Arsenide", symbol = "Ar", z = 33., a);

  density = 5.32 * g / cm3;
  G4Material* Galium_Arsenide = new G4Material (name = "Galium_Arsenide", density, ncomponents = 2);
  Galium_Arsenide ->AddElement (elGa, fractionmass = 50 * perCent);
  Galium_Arsenide ->AddElement (elAs, fractionmass = 50 * perCent);

  G4double z1, a1, density1;

  density1 = 2.328 * g / cm3;
  a1 = 28.086 * g / mole;
  G4Material* Silicon = new G4Material(name = "Silicon", z1 = 14., a1, density1);

  //
  // Sensor
  //

  G4double shape1_hx =  14.1 * mm;
  G4double shape1_hy =  14.1 * mm;
  G4double shape1_hz =  1000.*um;
  G4Box* solidShape1 =
    new G4Box("Sensor",
              shape1_hx / 2, shape1_hy / 2, shape1_hz / 2);

  G4LogicalVolume* logicShape1 =
    new G4LogicalVolume(solidShape1,         //its solid
                        Galium_Arsenide,     //its material
                        "Sensor");           //its name
  logicShape1->SetUserLimits(new G4UserLimits(5 * um));

  G4ThreeVector pos1 = G4ThreeVector(0, 0, shape1_hz / 2);
  new G4PVPlacement(0,                       //no rotation
                    pos1,                    //at position
                    logicShape1,             //its logical volume
                    "Sensor",                //its name
                    logicWorld,              //its mother  volume
                    false,                   //no boolean operation
                    0,                       //copy number
                    checkOverlaps);          //overlaps checking


  //
  // Shape 2
  //

  G4double shape2_hx = 14.1 * mm;
  G4double shape2_hy = 14.1 * mm;
  G4double shape2_hz = 300 * um;
  G4Box* solidShape2 =
    new G4Box ("Medipix_chip",                      //its name
               shape2_hx / 2, shape2_hy / 2,
               shape2_hz / 2);                 //its size

  G4LogicalVolume* logicShape2 =
    new G4LogicalVolume(solidShape2,        //its solid
                        Silicon,            //its material
                        "Medipix_chip");          //its name

  G4ThreeVector pos2 = G4ThreeVector(0, 0, -shape2_hz / 2);

  new G4PVPlacement(0,                       //no rotation
                    pos2,                    //at position
                    logicShape2,             //its logical volume
                    "Medipix_chip",                //its name
                    logicWorld,              //its mother  volume
                    false,                   //no boolean operation
                    0,                       //copy number
                    checkOverlaps);          //overlaps checking

  // Set Shape1 as scoring volume
  //
  fScoringVolume = logicShape1;

  //
  //always return the physical World
  //
  return physWorld;
}


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
