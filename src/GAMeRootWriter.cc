#include "GAMeRootWriter.hh"

GAMeRootWriter* GAMeRootWriter::pInstance = 0;

GAMeRootWriter* GAMeRootWriter::GetPointer() {
  if (pInstance == 0) pInstance = new GAMeRootWriter();
  return pInstance;
}

void GAMeRootWriter::Initialize(long runNumber) {

  char fname[30];
  sprintf(fname, "mega_%ld.root", runNumber);
  file = new TFile(fname, "RECREATE");

  tree = new TTree("tree", "tree");
  vecE = new std::vector<float>;
  vecX = new std::vector<float>;
  vecY = new std::vector<float>;
  vecZ = new std::vector<float>;
  tree->Branch("a", "float", &a);
  tree->Branch("e", "vector<float>", &vecE);
  tree->Branch("x", "vector<float>", &vecX);
  tree->Branch("y", "vector<float>", &vecY);
  tree->Branch("z", "vector<float>", &vecZ);
}

void GAMeRootWriter::Fill() {
  tree->Fill();
}

void GAMeRootWriter::Finalize() {
  file->cd();
  tree->Write();
  file->Close();
}


