#include "GAMePrimaryGeneratorAction.hh"

#include "G4LogicalVolumeStore.hh"
#include "G4LogicalVolume.hh"
#include "G4Box.hh"
#include "G4RunManager.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "G4SystemOfUnits.hh"
#include "Randomize.hh"
#include "TRandom3.h"

GAMePrimaryGeneratorAction::GAMePrimaryGeneratorAction()
  : G4VUserPrimaryGeneratorAction(),
    fParticleGun(0),
    fEnvelopeBox(0)
{
  G4int n_particle = 1;
  fParticleGun  = new G4ParticleGun(n_particle);

  // default particle kinematic
  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
  G4String particleName;
  G4double energy = 18 * keV;

  G4ThreeVector position = G4ThreeVector(0., 0., 18 * cm) ;
  G4ThreeVector momentumDirection = G4ThreeVector(0., 0., -1);
  G4ParticleDefinition* particle = particleTable->FindParticle(particleName = "gamma");
  fParticleGun->SetParticleDefinition(particle);
  fParticleGun->SetParticlePosition(position);
  fParticleGun->SetParticleMomentumDirection(momentumDirection);
  fParticleGun->SetParticleEnergy(energy);
}

GAMePrimaryGeneratorAction::~GAMePrimaryGeneratorAction()
{
  delete fParticleGun;
}

void GAMePrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{
  /*TRandom * gRandom = new TRandom3();
  gRandom->SetSeed(0);*/
  float x = 0;// + (gRandom->Rndm() - 0.5) * 10 * um;
  float y = 0;// + (gRandom->Rndm() - 0.5) * 10 * um;
  //G4cout << x<<"   " << y << G4endl;
  fParticleGun->SetParticlePosition(G4ThreeVector(x, y, 1 * cm));
  //gRandom->Delete();
  fParticleGun->GeneratePrimaryVertex(anEvent);
}

