#include "GAMeSteppingAction.hh"
#include "GAMeEventAction.hh"
#include "GAMeDetectorConstruction.hh"

#include "G4Step.hh"
#include "G4Event.hh"
#include "G4RunManager.hh"
#include "G4LogicalVolume.hh"
#include <iostream>

GAMeSteppingAction::GAMeSteppingAction(GAMeEventAction* eventAction)
: G4UserSteppingAction(),
  fEventAction(eventAction),
  fScoringVolume(0)
{}

GAMeSteppingAction::~GAMeSteppingAction()
{}

void GAMeSteppingAction::UserSteppingAction(const G4Step* step)
{
  if (!fScoringVolume) { 
    const GAMeDetectorConstruction* detectorConstruction
      = static_cast<const GAMeDetectorConstruction*>
        (G4RunManager::GetRunManager()->GetUserDetectorConstruction());
    fScoringVolume = detectorConstruction->GetScoringVolume();   
  }

  // get volume of the current step
  G4LogicalVolume* volume 
    = step->GetPreStepPoint()->GetTouchableHandle()
      ->GetVolume()->GetLogicalVolume();
      
  // check if we are in scoring volume
  if (volume != fScoringVolume) return;

  // collect energy deposited in this step
  G4double edepStep = step->GetTotalEnergyDeposit();

  G4ThreeVector pos = (step->GetPreStepPoint()->GetPosition() + step->GetPostStepPoint()->GetPosition() )/2; 
  //G4ThreeVector pos1 = step->GetPostStepPoint()->GetPosition(); 
  G4String partName = step->GetTrack()->GetDynamicParticle()->GetDefinition()->GetParticleName();
  //G4cout << "particle:   " << partName << G4endl;
  //G4cout << "position:   " << pos << G4endl;
  //G4cout << partName <<" " << step->GetTrack()->GetTrackID() << " " << /*"energy dep: " <<*/ edepStep / CLHEP::eV << " " << pos.getX()/ CLHEP::um<< " " << pos.getY()/ CLHEP::um<< " " << pos.getZ()/ CLHEP::um << G4endl;
  //G4cout << "track id:   " << step->GetTrack()->GetTrackID() << G4endl;


  //G4Track *aTrack = aStep->GetTrack() ;
  
  //G4double edepPos;
  fEventAction->AddEdep(edepStep/ CLHEP::eV, pos/ CLHEP::um);  
}
